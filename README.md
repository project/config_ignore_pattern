## INTRODUCTION

The Config ignore pattern module allows certain configuration to be ignored from
sync import and export based on the configuration object name.

The primary use case for this module is preventing the removal of configuration
from active storage during configuration import, e.g. Webforms created on a
production site.

## REQUIREMENTS

None.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

Configuration is ignored using regular expressions matched to configuration
object names. Ignore patterns are set in  `settings.php`.

In this example, all webforms created via the UI would not be exported during
`drush config:export` and any such UI-created webforms would not be deleted
during `drush config:import`.

```
/**
 * Ignored config name regex patterns.
 */
$settings['config_ignore_patterns'] = [
  '/^webform\.webform\.[a-z_]+$/',
];
```

Any other configuration that depends on configuration matching this pattern will
also be ignored.

Existing configuration in file/sync storage that matches ignore patterns will
still be imported and exported. This also allows configuration management of
selected configuration objects even when their names match ignore patterns.

For example, if all Webform entities are ignored using the above pattern, you
could still export and import config for a specific Webform if you add a
configuration file for it in your `config_sync_directory`.

The easiest way to do so is to perform a 'Single item' configuration export from
`/admin/config/development/configuration/single/export`.

You can enable debugging to see debug messages on
`/admin/config/development/configuration` and during `drush config:export` and
`drush config:import`.

```
/**
 * Config ignore pattern debugging.
 */
$settings['config_ignore_pattern_debug'] = TRUE;
```

## MAINTAINERS

Current maintainers for Drupal 10:

- Jeff Amaral (jeffam) - https://www.drupal.org/u/jeffam
- Aaron Froehlich (a-fro) - https://www.drupal.org/u/a-fro

