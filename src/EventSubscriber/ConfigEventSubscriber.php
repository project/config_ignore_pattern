<?php

namespace Drupal\config_ignore_pattern\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Subscriber for config importing and exporting.
 */
class ConfigEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new ConfigEventSubscriber object.
   */
  public function __construct(
    protected StorageInterface $activeStorage,
    protected StorageInterface $fileStorage,
    protected StateInterface $state,
    protected Settings $settings,
    protected MessengerInterface $messenger,
    protected array $ignorePatterns = []
  ) {
    $this->ignorePatterns = $this->settings::get('config_ignore_patterns', []);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ConfigEvents::STORAGE_TRANSFORM_EXPORT => 'onExportTransform',
      ConfigEvents::STORAGE_TRANSFORM_IMPORT => 'onImportTransform',
    ];
  }

  /**
   * The storage is transformed for exporting.
   *
   * Remove ignored patterns from the export, unless they exist in the file
   * storage.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    /** @var \Drupal\Core\Config\StorageInterface $storage */
    $storage = $event->getStorage();
    $ignored_config = [];

    foreach ($this->getIgnoredActiveConfig() as $config_name) {
      // Remove the outgoing config item, preventing it from being exported.
      $storage->delete($config_name);
      $ignored_config[] = $config_name;
    }

    $this->state->set('config_ignored_export', implode(PHP_EOL, $ignored_config));
  }

  /**
   * The storage is transformed for exporting.
   *
   * Remove ignored patterns from the import, unless they exist in the file
   * storage.
   *
   * This prevents deleting configuration during deployment and configuration
   * synchronization.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    /** @var \Drupal\Core\Config\StorageInterface $storage */
    $storage = $event->getStorage();
    $ignored_config = [];

    foreach ($this->getIgnoredActiveConfig() as $config_name) {
      // Set the incoming value to the active store value. This makes it
      // appear to be identical, thus ignoring it.
      $storage->write($config_name, $this->activeStorage->read($config_name));
      $ignored_config[] = $config_name;
    }

    $this->state->set('config_ignored_import', implode(PHP_EOL, $ignored_config));
  }

  /**
   * Gets the active configuration items that match the ignored patterns.
   *
   * - Dependencies of ignored config are also ignored.
   * - Skips config items in the sync store (e.g. config/sync).
   */
  protected function getIgnoredActiveConfig() {
    return array_filter($this->activeStorage->listAll(), function($active_config_name) {
      if ($this->fileStorage->exists($active_config_name)) {
        return FALSE;
      }

      $active_config_data = $this->activeStorage->read($active_config_name);
      $config_names = [ $active_config_name ];
      $config_names = array_merge($config_names, $active_config_data['dependencies']['config'] ?? []);

      foreach ($this->ignorePatterns as $pattern) {
        foreach ($config_names as $key => $config_name) {
          if (preg_match($pattern, $config_name) === 1) {
            if ($this->settings::get('config_ignore_pattern_debug', FALSE)) {
              $this->messenger->addMessage($this->t('Ignored <strong>@active_config_name</strong>: @descriptor <code>@pattern</code>.', [
                '@active_config_name' => $active_config_name,
                '@pattern' => $pattern,
                '@descriptor' => $config_names[0]=== $config_name ? $this->t('matches') : $this->t('depends on')
              ]));
            }

            return TRUE;
          }
        }
      }

      return FALSE;
    });
  }

}
